package src.Demo;

import src.Service.NumberService;

public class DemoService implements IDemoService{

    @Override
    public void execute() {
        NumberService ns = new NumberService();

        // Задача 2.1
        System.out.println("Задача 2.1");
        System.out.print("Плошадь круга: ");
        ns.circleArea(5.15858585187458148745);

        System.out.println();

        // Задача 2.2
        System.out.println("Задача 2.2");
        String strNumber1 = "0.1";
        String strNumber2 = "0.15";
        String strNumber3 = "0.25";
        System.out.println(strNumber1 + " + " + strNumber2 + " = " + strNumber3 + "?");
        System.out.println("Результат: " + ns.sum(strNumber1,strNumber2,strNumber3));

        System.out.println();

        System.out.println(strNumber1 + " + " + strNumber3 + " = " + strNumber2 + "?");
        System.out.println("Результат: " + ns.sum(strNumber1,strNumber3,strNumber2));

        System.out.println();

        // Задача 2.3
        System.out.println("Задача 2.3");

        int num1 = 5;
        int num2 = 7;
        int num3 = 20;
        System.out.print("Максимальным из трех чисел: " + num1 + ", " + num2 + ", " + num3 + ". Является: ");
        System.out.println(ns.findMax(num1,num2,num3));

        System.out.print("Минимальным из трех чисел: " + num1 + ", " + num2 + ", " + num3 + ". Является: ");
        System.out.println(ns.findMin(num1,num2,num3));

    }

}
