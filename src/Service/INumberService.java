package src.Service;

public interface INumberService {
    void circleArea(double radius);
    boolean sum(String number1, String number2, String number3);
    int findMax(int one, int two, int three);
    int findMin(int one, int two, int three);
}
