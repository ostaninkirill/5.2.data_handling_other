package src.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class NumberService implements INumberService{

    @Override
    public void circleArea(double radius) {
        System.out.println(new BigDecimal(radius).pow(2).multiply(new BigDecimal(Math.PI)).setScale(50, RoundingMode.HALF_UP));
    }

    @Override
    public boolean sum(String strNumber1, String strNumber2, String strNumber3) {
        // Учёт локаля пользователя
        if (strNumber1.contains(",") || strNumber2.contains(",") || strNumber3.contains(",")) {
            strNumber1 = strNumber1.replace(',', '.');
            strNumber2 = strNumber2.replace(',', '.');
            strNumber3 = strNumber3.replace(',', '.');
        }

        BigDecimal num1 = new BigDecimal(strNumber1);
        BigDecimal num2 = new BigDecimal(strNumber2);
        BigDecimal num3 = new BigDecimal(strNumber3);

        return num1.add(num2).compareTo(num3) == 0;
    }


    @Override
    public int findMax(int one, int two, int three) {
        return Math.max(one, Math.max(two,three));
    }

    @Override
    public int findMin(int one, int two, int three) {
        return Math.min(one, Math.min(two,three));
    }
}
